'use strict';

function pow(x, n){
var result = x;
for(var i = 0; i < n; i++)
 {
   result = result * x;
 }
   return(result);
}
console.log(pow(2, 3));

/* or */
function pow(x, n) {
  var result;
  result = x ** n;
  return result;
}
console.log(pow(x, n));
