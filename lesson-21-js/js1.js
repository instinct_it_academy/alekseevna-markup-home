'use strict'

var a = 1;
var b = 100;
var c = 3;
var d = 5;

for (var i = a; i <= b; i++) {
    if (i % c * d === 0) {
        console.log (i + 'Fuddy-Duddy');
        continue;
    }
    console.log(i)
}
